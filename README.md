# How to run 
1. ``npm i ``
2. run db queries in others/sql.db 
3. set .env values 
4. import postman json from others/paninti.postman_collection.json or https://www.getpostman.com/collections/b48fcf3c0a5aa8dcc485 
5. run apps: 
``nest start api-gateway`` 
``nest start product`` 
``nest start store`` 
``nest start user`` 


# Project structures and explanation 
This is monorepo app. it means that in this repore there are 4 apps. Api-gateway, product, store, and user. Other than that, there are 3 libraries that supports theses apps: bll, common, and dal. Basically, the apps is the apps. Api gateway serves as interfaces to user or apps, while product, store, and user serves their respectives roles as their names suggests. BLL libs contains logic, common libs contains files that used in either one of these apps or even libs, and dal contains db models.

  
apps/  
---api-gateway/  
------src/  
---------controllers/  
---product/   
------src/   
---------controllers/   
---store/   
------src/   
---------controllers/   
---api-user/   
------src/   
---------controllers/   
libs/   
---bll/   
------src/   
---------services/   
---common/   
------src/   
---------configs/   
---------filters 
---------guards/   
---------helpers/   
---------pipes/   
---------requests/   
---------strategies/        
---dal/   
------src/    
---------models/   


# Api gateway was built using: 
Node v10.15.3   
Nest.js 7   
Mysql 5   


# libraries used:  
hapi   
@hapi/joi   
@nestjs/axios   
@nestjs/common   
@nestjs/core  
@nestjs/jwt   
@nestjs/passport   
@nestjs/platform-express   
axios   
bcrypt  
class-transformer   
class-validator  
dotenv  
joi  
json-api-serializer  
moment   
mysql2   
passport   
passport-jwt   
reflect-metadata   
rimraf   
rxjs  
sequelize   
sequelize-typescript   
source-map-support   



