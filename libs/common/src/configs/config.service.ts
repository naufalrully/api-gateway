import * as Joi from '@hapi/joi';
import * as dotenv from 'dotenv';
import * as fs from 'fs';
import { Product } from 'libs/dal/src/models/Product';
import { Store } from 'libs/dal/src/models/Store';
import { User } from 'libs/dal/src/models/User';
import sequelize = require('sequelize');
import { Sequelize } from 'sequelize-typescript';

const Op = sequelize.Op;
const operatorsAliases = {
  eq: Op.eq,
  ne: Op.ne,
  gte: Op.gte,
  gt: Op.gt,
  lte: Op.lte,
  lt: Op.lt,
  not: Op.not,
  in: Op.in,
  notIn: Op.notIn,
  is: Op.is,
  like: Op.like,
  notLike: Op.notLike,
  iLike: Op.iLike,
  notILike: Op.notILike,
  regexp: Op.regexp,
  notRegexp: Op.notRegexp,
  iRegexp: Op.iRegexp,
  notIRegexp: Op.notIRegexp,
  between: Op.between,
  notBetween: Op.notBetween,
  overlap: Op.overlap,
  contains: Op.contains,
  contained: Op.contained,
  adjacent: Op.adjacent,
  strictLeft: Op.strictLeft,
  strictRight: Op.strictRight,
  noExtendRight: Op.noExtendRight,
  noExtendLeft: Op.noExtendLeft,
  and: Op.and,
  or: Op.or,
  any: Op.any,
  all: Op.all,
  values: Op.values,
  col: Op.col,
};

export type EnvConfig = Record<string, string>;
export class ConfigService {
  private readonly envConfig: EnvConfig;  

  constructor() {
    const config = dotenv.parse(fs.readFileSync(`.env`));
    this.envConfig = this.validateInput(config);    
  }

  /**
   * Ensures all needed variables are set, and returns the validated JavaScript object
   * including the applied default values.
   */
  private validateInput(envConfig: EnvConfig): EnvConfig {
    const envVarsSchema: Joi.ObjectSchema = Joi.object({
      ENV: Joi.string()
        .valid('development', 'production', 'test', 'provision')
        .default('development'),      
      JWT_ALGORITHM: Joi.string().required(),
      JWT_KEY_FOLDER: Joi.string().required(),
      JWT_DEFAULT_EXPIRE_TIME: Joi.number().required(),
      JWT_SECRET_KEY: Joi.string().required(),
      JWT_PUBLIC_KEY: Joi.string().required(),      

      BASE_URL: Joi.string().required(),
      API_GATEWAY_PORT: Joi.number().required(),
      USER_APP_PORT: Joi.number().required(),
      STORE_APP_PORT: Joi.number().required(),
      PRODUCT_APP_PORT: Joi.number().required(),
    });

    const { error, value: validatedEnvConfig } = envVarsSchema.validate(
      envConfig,
    );
    if (error) {
      throw new Error(`Config validation error: ${error.message}`);
    }
    return validatedEnvConfig;
    
  }

  public get(key: string, isNumber: boolean = false): any {
    const res = this.envConfig[key];
    if (isNumber) {
      Number(res)
    }
    return res;
  }

  public dbConnection(config: ConfigService = new ConfigService()) {
    const db = new Sequelize({
      operatorsAliases,
      dialect: 'mysql',
      // dialect: config.get('DB_CONNECTION'),
      // logging: config.get('ENV') === 'PRODUCTION' ? false : true,
      logQueryParameters: true,
      replication: {
        read: [
          {
            // host: config.get('DB_READ_HOST'),
            host: '127.0.0.1',
            // port: config.get('DB_READ_PORT', true),
            port: '3306',
            // username: config.get('DB_READ_USERNAME'),
            username: 'root',
            // password: config.get('DB_READ_PASSWORD'),
            password: 'Admin123!',
            // database: config.get('DB_READ_NAME'),
            database: 'store',
          },
        ],
        write: {
          // host: config.get('DB_READ_HOST'),
          host: '127.0.0.1',
          // port: config.get('DB_READ_PORT', true),
          port: '3306',
          // username: config.get('DB_READ_USERNAME'),
          username: 'root',
          // password: config.get('DB_READ_PASSWORD'),
          password: 'Admin123!',
          // database: config.get('DB_READ_NAME'),
          database: 'store',  
        },
      },
      // isolationLevel: 'READ UNCOMMITTED',
      query: {
        // raw: true,
      },
      define: {
        underscored: true,
      },
    });
    db.addModels([User, Store, Product]);
    // db.sync()
    return db
  }  
}
