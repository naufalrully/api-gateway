import { HttpModule, Module } from '@nestjs/common';
import { ConfigService } from './configs/config.service';
import { JwtAuthGuard } from './guards/jwt-auth.guard';
import { PasswordHasher } from './helpers/password.hasher';
import { RequestHelper } from './helpers/request.helper';
import { ValidationPipe } from './pipes/validation.pipe';
import { ProductRequest } from './requests/product.request';
import { StoreRequest } from './requests/store.request';
import { JwtStrategy } from './strategies/jwt.strategy';

@Module({
  imports:[
    HttpModule,    
  ],
  providers: [
    ProductRequest,
    StoreRequest,    
    ValidationPipe,
    ConfigService,
    PasswordHasher,
    JwtAuthGuard,
    JwtStrategy,
    RequestHelper,
  ],
  exports: [
    ProductRequest,
    StoreRequest,    
    ValidationPipe,
    ConfigService,
    PasswordHasher,
    JwtAuthGuard,
    JwtStrategy,
    RequestHelper,
  ],
})
export class CommonModule {}
