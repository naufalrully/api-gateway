
import { PipeTransform, Injectable, ArgumentMetadata, BadRequestException, HttpException } from '@nestjs/common';
import { validate } from 'class-validator';
import { plainToClass } from 'class-transformer';

@Injectable()
export class ValidationPipe implements PipeTransform<any> {
  async transform(value: any, { metatype }: ArgumentMetadata) {  
    if (!metatype || !this.toValidate(metatype)) {
      return value;
    }
    const object = plainToClass(metatype, value);
    const errors = await validate(object);
    if (errors.length > 0) {
      const mappedErrors = await Promise.all(errors.map(async (error) => {
        const newError = {}
        if (error.children.length == 0) {
          newError[error.property] = Object.values(error.constraints)
          return newError
        }
        if (error.children[0]) {
          return this.getChildrenConstraint(error.children[0])
        }

      }))
      throw new HttpException(mappedErrors, 422);
    }
    return value;
  }

  private toValidate(metatype: Function): boolean {
    const types: Function[] = [String, Boolean, Number, Array, Object];
    return !types.includes(metatype);
  }

  private getChildrenConstraint(children: any) {
    if (children.constraints) {
      const newError = {}
      newError[children.property] = Object.values(children.constraints)
      return newError
    }
    const grandChildren = children.children[0]
    return this.getChildrenConstraint(grandChildren)
  }
}
