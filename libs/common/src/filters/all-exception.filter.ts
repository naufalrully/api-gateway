import { ExceptionFilter, Catch, ArgumentsHost, HttpException, HttpStatus } from '@nestjs/common';

@Catch()
export class AllExceptionsFilter implements ExceptionFilter {
  catch(exception: HttpException, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse();
    const request = ctx.getRequest();    
    const status =
      exception instanceof HttpException
        ? exception.getStatus()
        : HttpStatus.INTERNAL_SERVER_ERROR;                    
    let message:any = exception.message || exception.message || exception.message || 'internal server error'

    if (status === 401) {            
      // message = 'Anda harus login terlebih dahulu'
    } 
    
    if(status === 422){
      message = exception.getResponse()      
    }

    response.status(status).json({
      error: {
        statusCode: status,
        timestamp: new Date().toISOString(),
        path: request.url,
        detail: message,        
      },
    });
  }
}
