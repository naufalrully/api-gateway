import { CanActivate, ExecutionContext, Injectable, UnauthorizedException } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { AuthService } from 'libs/bll/src/services/auth.service';
import { Observable } from 'rxjs';

@Injectable()
export class JwtAuthGuard extends AuthGuard('jwt')  implements CanActivate{
  constructor(    
    private authService: AuthService) {
    super();
  }
  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const request = context.switchToHttp().getRequest();
    const auth = request.headers.authorization
    const bearer = 'Bearer '
    if(!auth){
      throw new UnauthorizedException()
    }
    const token = auth.split(bearer).pop()        
    if(!this.authService.verifyToken(token)){      
      throw new UnauthorizedException()      
    }
    request.user = this.authService.decodeToken(token)
    return true
  }
}