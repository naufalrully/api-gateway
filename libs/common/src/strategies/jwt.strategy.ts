import { ExtractJwt, Strategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable } from '@nestjs/common';
import { ConfigService } from '../configs/config.service';
import * as fs from 'fs';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy, 'jwt') {
  constructor(
    private readonly configService: ConfigService, 
  ) {
    super({      
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      secretOrKey:fs.readFileSync(configService.get('JWT_KEY_FOLDER') + configService.get('JWT_SECRET_KEY')),
      privateKey: fs.readFileSync(configService.get('JWT_KEY_FOLDER') + configService.get('JWT_SECRET_KEY')),
      publicKey: fs.readFileSync(configService.get('JWT_KEY_FOLDER') + configService.get('JWT_PUBLIC_KEY')),
      verifyOptions: { 
        algorithms: [configService.get('JWT_ALGORITHM')] 
      },
      signOptions: { 
        expiresIn: configService.get('JWT_DEFAULT_EXPIRE_TIME'), 
        algorithm:configService.get('JWT_ALGORITHM') 
      },
    });
  }

  async validate(payload: any) {    
    return { userId: payload.userId, phone: payload.phone, name: payload.name, type: payload.type };
  }
}
