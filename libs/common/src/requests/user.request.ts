import { Type } from 'class-transformer';
import { IsNotEmpty, IsBoolean, IsOptional, IsArray, ValidateNested, IsEmail } from 'class-validator';
import { User } from 'libs/dal/src/models/User';
import { StoreRequest } from './store.request';


export class UpdateRequest {  

  @IsNotEmpty()
  userName: string
  
  @IsNotEmpty()
  phone: string
  
  @IsOptional()
  imageUrl: string
}

export class LoginRequest {
  @IsNotEmpty()
  password: string

  @IsNotEmpty()
  @IsEmail()
  email: string
}

export class RegisterRequest {
  @IsNotEmpty()
  password: string

  @IsNotEmpty()
  @IsEmail()
  email: string

  @IsNotEmpty()
  userName: string
  
  @IsNotEmpty()
  phone: string
  
  @IsOptional()
  imageUrl: string

  @IsNotEmpty()
  @ValidateNested({ each: true })
  @Type(() => StoreRequest)
  store: StoreRequest
}


export class UserViewModel {
  id:number
  userName:string
  phone:string
  email:string
  imageUrl:string
  createdAt:Date
  updatedAt:Date
  isDeleted:boolean  

  constructor(rawData:User) {
    this.id = rawData.id
    this.userName = rawData.userName    
    this.phone = rawData.phone
    this.email = rawData.email    
    this.imageUrl = rawData.imageUrl    
    this.createdAt = rawData.createdAt    
    this.updatedAt = rawData.updatedAt    
    this.isDeleted = rawData.isDeleted    
  }

}
