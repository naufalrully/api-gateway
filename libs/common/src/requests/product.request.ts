import { IsNotEmpty, IsBoolean, IsOptional, IsArray, ValidateNested, IsString } from 'class-validator';


export class ProductRequest {
  @IsNotEmpty()
  name: string
  
  @IsNotEmpty()
  @IsString()
  description:string  
  
  @IsOptional()  
  @IsString()
  imageUrl:string

  @IsNotEmpty()  
  userId  :number

  @IsNotEmpty()  
  storeId  :number

}