import { IsNotEmpty, IsBoolean, IsOptional, IsArray, ValidateNested, IsString } from 'class-validator';


export class StoreRequest {
  @IsNotEmpty()
  name: string
  
  @IsNotEmpty()
  @IsString()
  address:string

  @IsNotEmpty()
  @IsString()
  phone:string

  @IsNotEmpty()
  @IsString()
  email:string

  @IsOptional()  
  @IsString()
  imageUrl:string

  @IsOptional()  
  userId :number 

}