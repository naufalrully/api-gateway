import { Injectable } from '@nestjs/common';
const bcrypt = require('bcrypt')
const saltRounds = 10

@Injectable()
export class PasswordHasher {

  async hash(password:string) {    
    return await  bcrypt.hash(password, saltRounds)
  }

  async compare(reqPassword:string, password:string) {
    return await bcrypt.compare(reqPassword, password)
  }

}
