export const PRODUCT_SERVICE = 'product'
export const USER_SERVICE = 'user'
export const STORE_SERVICE = 'store'

export const METHOD_GET = 'GET'
export const METHOD_PUT = 'PUT'
export const METHOD_POST = 'POST'
export const METHOD_DELETE = 'DELETE'

export const ACTION_PRODUCT = 'product'
export const ACTION_USER = 'user'
export const ACTION_USER_LOGIN = 'user/login'
export const ACTION_USER_REGISTER = 'user/register'
export const ACTION_STORE = 'store'