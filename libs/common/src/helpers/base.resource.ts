import * as JSONAPISerializer from 'json-api-serializer'

export class BaseResource {
  constructor(data: any, className: string) {
    const serializer = new JSONAPISerializer()    

      serializer.register(className, {
        id: 'id',
        relationships: {                  
          user: {
            type: 'user',
          },
        },
      })

      serializer.register('user', {
        id: 'id',
        blacklist: [          
          'password'
        ],
        relationships: {        
          store: {
            type: 'store',
          },
        },
      })

      serializer.register('product', {
        id: 'id',
        blacklist: [          
          'password'
        ],
        relationships: {        
          store: {
            type: 'store',
          },
          user: {
            type: 'user',
          },
        },
      })  

      serializer.register('store', {
        id: 'id',
        blacklist: [                    
        ],
        relationships: {                  
          user: {
            type: 'user',
          },
        },
      })  

    return serializer.serializeAsync(className, data)
      .then((data: any) => {
        return data
      })
  }
}
