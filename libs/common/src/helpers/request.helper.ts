import { forwardRef, HttpService, HttpStatus, Inject, Injectable, NotFoundException } from "@nestjs/common";
import { AxiosError, AxiosResponse } from "axios";
import { response } from "express";
import { Observable } from "rxjs";
import { ConfigService } from "../configs/config.service";
import { METHOD_DELETE, METHOD_GET, METHOD_POST, METHOD_PUT, PRODUCT_SERVICE, STORE_SERVICE, USER_SERVICE } from "./constant";
import { circularToJSON } from "./helper";

@Injectable()
export class RequestHelper {
  private baseUrl:string
  constructor(
    private httpService: HttpService,
    @Inject(forwardRef(() => ConfigService))  
    private configService:ConfigService
    ) {
      this.baseUrl = `http://${this.configService.get('BASE_URL')}`
    }


  async makeRequest(
    token:string,
    method:string,
    service:string,    
    action:string,
    id?:number,
    params:any = null,
    body?:any
  ){
    let data
    let url = `${this.baseUrl}:`
    switch(service){      
      case PRODUCT_SERVICE:             
        url = `${url}${this.configService.get('PRODUCT_APP_PORT')}/`      
      break
      case USER_SERVICE:
        url = `${url}${this.configService.get('USER_APP_PORT')}/`
      break
      case STORE_SERVICE:
        url = `${url}${this.configService.get('STORE_APP_PORT')}/`
      break
    }
    
    url = `${url}${action}/`
    if(!!id){
      url = `${url}${id}`
    }
      

    switch(method){
      case METHOD_GET:
        data =  this.httpService.get(
          url,
          {
            headers:{
              Authorization: token
            },
            params,
          }
        )
        break
      case METHOD_POST:
        data = this.httpService.post(
          url,
          body,
          {
            headers:{
              Authorization: token
            },
            params,
          }                                        
        )
        break
      case METHOD_PUT:
        data = this.httpService.put(
          url,
          body,
          {
            headers:{
              Authorization: token
            },
            params,
          }                                        
        )
        break
      case METHOD_DELETE:
        data = this.httpService.delete(
          url,          
          {
            headers:{
              Authorization: token
            },
            params,
          }                                        
        )
        break
    }    
      
    return await data
    .toPromise()
    .then((response: AxiosResponse) => {                  
      if (
        response.status == HttpStatus.OK ||
        response.status == HttpStatus.ACCEPTED ||
        response.status == HttpStatus.CREATED
      ){
        return circularToJSON(response.data)
      }
    })
    .catch((reason: AxiosError) => {
      if(reason.response!){
        return circularToJSON(reason.response.data)
      }      
      throw new Error('failed to make request')
    })    
  }  
  
}