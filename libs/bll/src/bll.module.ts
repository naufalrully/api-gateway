import { HttpModule, Module } from '@nestjs/common';
import { CommonModule } from 'app/common';
import { ConfigService } from 'app/common/configs/config.service';
import { AuthService } from 'libs/bll/src/services/auth.service';
import { UserModule } from 'apps/user/src/user.module';
import { DalModule } from 'libs/dal/src';
import { UserService } from './services/user.service';
import { JwtModule, JwtService } from '@nestjs/jwt';
import * as fs from 'fs';
import { JwtStrategy } from 'app/common/strategies/jwt.strategy';
import { PassportModule } from '@nestjs/passport';
import { StoreService } from './services/store.service';
import { ProductService } from './services/product.service';

@Module({
  imports: [
    HttpModule,
    DalModule,    
    CommonModule,      
    ConfigService,      
    PassportModule.register({ defaultStrategy: 'jwt' }),
    JwtModule.registerAsync({
      imports: [
        ConfigService,
        CommonModule,
      ],
      useFactory: async (configService: ConfigService) => ({                
        privateKey: fs.readFileSync(configService.get('JWT_KEY_FOLDER') + configService.get('JWT_SECRET_KEY')),
        publicKey: fs.readFileSync(configService.get('JWT_KEY_FOLDER') + configService.get('JWT_PUBLIC_KEY')),
        verifyOptions: { 
          algorithms: [configService.get('JWT_ALGORITHM')] 
        },
        signOptions: { 
          expiresIn: configService.get('JWT_DEFAULT_EXPIRE_TIME'), 
          algorithm:configService.get('JWT_ALGORITHM') 
        },
      }),
      inject: [ConfigService],      
    }),    
  ],
  providers: [
    AuthService,
    UserService,    
    JwtStrategy,
    StoreService,
    ProductService,
  ],
  exports: [
    AuthService,
    UserService,    
    StoreService,
    ProductService,
  ],
})
export class BllModule {}
