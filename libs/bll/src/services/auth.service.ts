import { Injectable, BadRequestException, HttpService, Inject, forwardRef } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import * as bcrypt from 'bcrypt'
import { ConfigService } from 'app/common/configs/config.service';
import { UserService } from 'libs/bll/src/services/user.service';

@Injectable()
export class AuthService {
  private saltRounds = 10;  

  constructor(
    private readonly config: ConfigService, 
    @Inject(forwardRef(() => UserService))    
    private userService: UserService, 
    private jwtService: JwtService) {    
  }

  async createToken(payload: any) {    
    return {
      token: this.jwtService.sign(payload, {
        expiresIn:'360d'
      }),
    }
  }

  verifyToken(tokenRaw:string) {
    try {
      const token = tokenRaw.replace('Bearer ', '')
      this.jwtService.verify(token)      
      return true
    } catch (error) {
      return false
    }
  }

  decodeToken(tokenRaw:string) {
    const token = tokenRaw.replace('Bearer ', '')
    return this.jwtService.decode(token)
  }

  async validateUser(email: string, password: string): Promise<any> {
    const user = await this.userService.findOneByEmail(email)
    const passwordCheck = await bcrypt.compare(password, user.password)    

    if (user && passwordCheck) {
      const { password, ...result } = user;
      return result;
    }
  }
}
