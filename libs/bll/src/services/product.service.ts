import { forwardRef, HttpService, Inject, Injectable, NotFoundException, UnprocessableEntityException } from "@nestjs/common";
import { ConfigService } from "app/common/configs/config.service";
import { ProductRequest } from "app/common/requests/product.request";
import { number } from "joi";
import { AuthService } from "libs/bll/src/services/auth.service";
import { Product } from "libs/dal/src/models/Product";
import { Store } from "libs/dal/src/models/Store";
import { User } from "libs/dal/src/models/User";
import moment = require("moment");
import { Transaction, Sequelize as sequelize, Op, QueryTypes } from 'sequelize'

@Injectable()
export class ProductService {
  private db

  constructor(
    private config: ConfigService,
    private httpService: HttpService,    
    @Inject(forwardRef(() => AuthService))    
    private authService: AuthService,
  ) {
    this.db = config.dbConnection()
  }

  async findAll(param:any, includeDeleted: boolean = false){    
    const whereQuery:{[k: string]: any} = {      
      isDeleted:includeDeleted,
    }
    const includeQuery:{[k: string]: any} = []
    

    if(param.userId){
      whereQuery.userId = param.userId
    }
    if(param.storeId){
      whereQuery.storeId = param.storeId
    }
    if(!!param.name){
      whereQuery.userName = sequelize.where(
        sequelize.fn('LOWER', sequelize.col('user_name')),
        {
          [Op.like]: `%${param.name}%`,
        },
      )
    }
    if (!!param.offset) param.offset = Number(param.offset)
    if (!!param.limit) param.limit = Number(param.limit)
    if (!!param.sort) {
      param.sort = param.sort.replace(' ', '')
      param.sort = param.sort.split(',')
      param.sort = param.sort.map((element:string) => {
        if (element.includes('-')) {
          return [element.replace('-', ''), 'DESC']
        }
        return [element, 'ASC']
      })
    }          
    if(!!param.include){
      const paramInclude: string[] = param.include            
      
      if(paramInclude.includes('user')){
        includeQuery.push(
          {
              model: User, 
              required: true,
              attributes:{
                exclude:['password']
              },
              where:{
                isDeleted:includeDeleted,
              },        
          },         
        )
      }
      if(paramInclude.includes('store')){
        includeQuery.push(
          {
              model: Store, 
              required: true,              
              where:{
                isDeleted:includeDeleted,
              },        
          },         
        )
      }
    }      

    let query = Product.findAll({                 
      where:whereQuery,
      include:includeQuery,
      order:param.sort,
      offset:param.offset,
      limit:param.limit,      
    })    


    return await query
  }

  async findOne(
    id:number, 
    param:any = {}, 
    isDeleted: boolean = false, 
    isThrow: boolean = false
  ){
    const includeQuery = []
    if(!!param.include){
      const paramInclude: string[] = param.include            
      
      if(paramInclude.includes('user')){
        includeQuery.push(
          {
              model: User, 
              required: true,              
              attributes:{
                exclude:['password']
              },              
              where:{
                isDeleted,
              },
          },         
        )
      }

      if(paramInclude.includes('store')){
        includeQuery.push(
          {
              model: Store, 
              required: true,                                   
              where:{
                isDeleted,
              },
          },         
        )
      }
    }
    const data =  await Product.findOne(
      {
        where:{
          id,
          isDeleted,
        },
        include:includeQuery,
      }
    )

    if (!!data){
      if (data.isDeleted && isThrow){
        throw new NotFoundException()  
      }
    } else{
      if (isThrow){
        throw new NotFoundException()  
      }
    }      

    return data
  }

  async findOneByUser(
    userId:number, 
    param:any, 
    isDeleted: boolean = false, 
    isThrow: boolean = false
  ){
    const includeQuery = []
    if(!!param.include){
      const paramInclude: string[] = param.include            
      
      if(paramInclude.includes('user')){
        includeQuery.push(
          {
            model: User, 
            required: true,              
            attributes:{
              exclude:['password']
            },              
            where:{
              isDeleted,
            },
          },         
        )
      }
    }
    const data =  await Product.findOne(
      {
        where:{
          userId,
        },
        include:includeQuery,
      }
    )

    if (!!data){
      if (data.isDeleted && isThrow){
        throw new NotFoundException()  
      }
    } else{
      if (isThrow){
        throw new NotFoundException()  
      }
    }      

    return data
  }

  async preCreate(
    user:any, 
    rawReq: ProductRequest
  ){    
    const req: ProductRequest = rawReq
    req.userId = user.id    
    
    return req
  }

  async create(
    user:any, 
    rawReq: ProductRequest
  ){
    const req = await this.preCreate(user, rawReq)

    let transaction
    let data
    try {
      transaction = await this.db.transaction({
        isolationLevel: Transaction.ISOLATION_LEVELS.READ_UNCOMMITTED,
      })      
          
      data = await Product.create(req, {transaction})

      await transaction.commit()
    } catch(error){
      await transaction.rollback()
      throw new Error()
    }

    return data 
  }

  async preUpdate(
    user:any,     
    rawReq: ProductRequest,
    product:Product,
  ){        
    if(user.id != product.userId){
      throw new NotFoundException()
    }
    product.name = rawReq.name
    product.description = rawReq.description    
    product.imageUrl = rawReq.imageUrl    

    return product
  }

  async update(
    user:any, 
    id:number,
    rawReq: ProductRequest,
  ){
    let product = await this.findOne(id, {}, false, true)
    product = await this.preUpdate(user, rawReq, product)

    let transaction
    let data
    try {
      transaction = await this.db.transaction({
        isolationLevel: Transaction.ISOLATION_LEVELS.READ_UNCOMMITTED,
      })      
      
      data = await product.update({
        name: product.name,
        description: product.description,    
        imageUrl: product.imageUrl,    
      }, {transaction})        
      

      await transaction.commit()
    } catch(error){
      await transaction.rollback()
      throw new Error()
    }

    return data 
  }

  async preDelete(
    user:any,         
    Product:Product,
  ){
    if(user.id != Product.userId){
      throw new NotFoundException()
    }    

    return Product
  }

  async delete(
    user:any,
    id:number
  ){
    let product = await this.findOne(id, {}, false, true)
    product = await this.preDelete(user, product)

    let transaction
    let data
    try {
      transaction = await this.db.transaction({
        isolationLevel: Transaction.ISOLATION_LEVELS.READ_UNCOMMITTED,
      })            
      
      data = await product.update({
        isDeleted:true
      }, {
        transaction,
      })
      

      await transaction.commit()
    } catch(error){
      await transaction.rollback()
      throw new Error()
    }

    return data 
  }

}