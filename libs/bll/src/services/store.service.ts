import { forwardRef, HttpService, Inject, Injectable, NotFoundException, UnprocessableEntityException } from "@nestjs/common";
import { ConfigService } from "app/common/configs/config.service";
import { StoreRequest } from "app/common/requests/store.request";
import { number } from "joi";
import { AuthService } from "libs/bll/src/services/auth.service";
import { Store } from "libs/dal/src/models/Store";
import { User } from "libs/dal/src/models/User";
import moment = require("moment");
import { Transaction, Sequelize as sequelize, Op, QueryTypes } from 'sequelize'

@Injectable()
export class StoreService {
  private db

  constructor(
    private config: ConfigService,        
  ) {
    this.db = config.dbConnection()
  }

  async findAll(param:any, includeDeleted: boolean = false){    
    const whereQuery:{[k: string]: any} = {      
      isDeleted:includeDeleted,
    }
    const includeQuery:{[k: string]: any} = []
    

    if(param.userId){
      whereQuery.userId = param.userId
    }
    if(!!param.name){
      whereQuery.userName = sequelize.where(
        sequelize.fn('LOWER', sequelize.col('user_name')),
        {
          [Op.like]: `%${param.name}%`,
        },
      )
    }
    if (!!param.offset) param.offset = Number(param.offset)
    if (!!param.limit) param.limit = Number(param.limit)
    if (!!param.sort) {
      param.sort = param.sort.replace(' ', '')
      param.sort = param.sort.split(',')
      param.sort = param.sort.map((element:string) => {
        if (element.includes('-')) {
          return [element.replace('-', ''), 'DESC']
        }
        return [element, 'ASC']

      })
    }          
    if(!!param.include){
      const paramInclude: string[] = param.include            
      
      if(paramInclude.includes('user')){
        includeQuery.push(
          {
              model: User, 
              required: true,
              attributes:{
                exclude:['password']
              },
              where:{
                isDeleted:includeDeleted,
              },        
          },         
        )
      }
    }
    

    let query = Store.findAll({                 
      where:whereQuery,
      include:includeQuery,
      order:param.sort,
      offset:param.offset,
      limit:param.limit,      
    })    


    return await query
  }

  async findOne(
    id:number, 
    param:any, 
    isDeleted: boolean = false, 
    isThrow: boolean = false
  ){
    const includeQuery = []
    if(!!param.include){
      const paramInclude: string[] = param.include            
      
      if(paramInclude.includes('user')){
        includeQuery.push(
          {
              model: User, 
              required: true,              
              attributes:{
                exclude:['password']
              },              
              where:{
                isDeleted,
              },
          },         
        )
      }
    }
    const data =  await Store.findOne(
      {
        where:{
          id,
          isDeleted,
        },
        include:includeQuery,
      }
    )

    if (!!data){
      if (data.isDeleted && isThrow){
        throw new NotFoundException()  
      }
    } else{
      if (isThrow){
        throw new NotFoundException()  
      }
    }      

    return data
  }

  async findOneByUser(
    userId:number, 
    param:any, 
    isDeleted: boolean = false, 
    isThrow: boolean = false
  ){
    const includeQuery = []
    if(!!param.include){
      const paramInclude: string[] = param.include            
      
      if(paramInclude.includes('user')){
        includeQuery.push(
          {
            model: User, 
            required: true,              
            attributes:{
              exclude:['password']
            },              
            where:{
              isDeleted,
            },
          },         
        )
      }
    }
    const data =  await Store.findOne(
      {
        where:{
          userId,
        },
        include:includeQuery,
      }
    )

    if (!!data){
      if (data.isDeleted && isThrow){
        throw new NotFoundException()  
      }
    } else{
      if (isThrow){
        throw new NotFoundException()  
      }
    }      

    return data
  }

  async preCreate(
    user:any, 
    rawReq: StoreRequest
  ){    
    const req: StoreRequest = rawReq
    if(!!req.userId){
      req.userId = user.id
    }

    const isStoreExists = await this.findOneByUser(user.id, {}, false, false)
    if (!!isStoreExists){
      throw new UnprocessableEntityException('user telah memiliki toko')
    }
    
    return req
  }

  async create(
    user:any, 
    rawReq: StoreRequest
  ){
    const req = await this.preCreate(user, rawReq)

    let transaction
    let data
    try {
      transaction = await this.db.transaction({
        isolationLevel: Transaction.ISOLATION_LEVELS.READ_UNCOMMITTED,
      })      
            
      data = await Store.create(req, {transaction})

      await transaction.commit()
    } catch(error){
      await transaction.rollback()
      throw new Error()
    }

    return data 
  }

  async preUpdate(
    user:any,     
    rawReq: StoreRequest,
    store:Store,
  ){        
    if(user.id != store.userId){
      throw new NotFoundException()
    }    
    store.name = rawReq.name
    store.email = rawReq.email
    store.phone = rawReq.phone
    store.imageUrl = rawReq.imageUrl
    store.address = rawReq.address

    return store
  }

  async update(
    user:any, 
    id:number,
    rawReq: StoreRequest,
  ){
    let store = await this.findOne(id, {}, false, true)
    store = await this.preUpdate(user, rawReq, store)

    let transaction
    let data
    try {
      transaction = await this.db.transaction({
        isolationLevel: Transaction.ISOLATION_LEVELS.READ_UNCOMMITTED,
      })      
      
      data = await store.update({
        name : store.name,
        email : store.email,
        phone : store.phone,
        imageUrl : store.imageUrl,
        address : store.address,
      }, {
        transaction,
      })        
      

      await transaction.commit()
    } catch(error){
      await transaction.rollback()
      throw new Error()
    }

    return data 
  }

  async preDelete(
    user:any,         
    store:Store,
  ){
    if(user.id != store.userId){
      throw new NotFoundException()
    }    

    return store
  }

  async delete(
    user:any,
    id:number
  ){
    let store = await this.findOne(id, {}, false, true)
    store = await this.preDelete(user, store)

    let transaction
    let data
    try {
      transaction = await this.db.transaction({
        isolationLevel: Transaction.ISOLATION_LEVELS.READ_UNCOMMITTED,
      })            
      
      data = await store.update({
        isDeleted:true
      }, {
        transaction,
      })
      

      await transaction.commit()
    } catch(error){
      await transaction.rollback()
      throw new Error()
    }

    return data 
  }

}