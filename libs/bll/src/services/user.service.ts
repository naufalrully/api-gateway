import { forwardRef, HttpService, Inject, Injectable, NotFoundException, UnprocessableEntityException } from "@nestjs/common";
import { ConfigService } from "app/common/configs/config.service";
import { ACTION_STORE, METHOD_POST, STORE_SERVICE } from "app/common/helpers/constant";
import { PasswordHasher } from "app/common/helpers/password.hasher";
import { RequestHelper } from "app/common/helpers/request.helper";
import { StoreRequest } from "app/common/requests/store.request";
import { LoginRequest, RegisterRequest, UpdateRequest } from "app/common/requests/user.request";
import { AuthService } from "libs/bll/src/services/auth.service";
import { User } from "libs/dal/src/models/User";
import moment = require("moment");
import { Transaction } from 'sequelize'

@Injectable()
export class UserService {
  private db

  constructor(
    private config: ConfigService,
    private readonly requestHelper: RequestHelper,
    private passwordHasher: PasswordHasher,
    @Inject(forwardRef(() => AuthService))    
    private authService: AuthService,
  ) {
    this.db = config.dbConnection()
  }

  // async findAll(): Promise<Observable<AxiosResponse<any[]>>> {    
  //   const data = await this.httpService.get('http://127.0.0.1:3001/user').toPromise()    
  //   return data.data;    
  // }

  async findAll(){
    return await User.findAll(
      {
        where:{
          isDeleted:false,
        }
      }
    )
  }

  async findOne(id:number, isDeleted: boolean = false, isThrow: boolean = false){
    const data =  await User.findOne(
      {
        where:{
          id,
        }
      }
    )

    if (!!data){
      if (data.isDeleted && isThrow){
        throw new NotFoundException()  
      }
    } else{
      if (isThrow){
        throw new NotFoundException()  
      }
    }      

    return data
  }

  async findOneByEmail(email:string, isDeleted: boolean = false, isThrow: boolean = false){
    const data =  await User.findOne(
      {
        where:{
          email,
        }
      }
    )

    if (!!data){
      if (data.isDeleted && isThrow){
        throw new NotFoundException()  
      }
    } else{
      if (isThrow){
        throw new NotFoundException()  
      }
    }

    return data
  }

  async preCreate(raw: RegisterRequest){
    const req = raw    
    req.password = await this.passwordHasher.hash(raw.password)

    const isEmailUsed = await this.findOneByEmail(req.email, false, false)
    if(!!isEmailUsed){
      throw new UnprocessableEntityException('email has been used')
    }
    
    return req
  }

  async preUpdate(
    raw: UpdateRequest,
    user: User
  ){
    const req:User = user
    req.phone = raw.phone    
    req.imageUrl = raw.imageUrl
    req.userName = raw.userName    

    return req
  }

  async create(    
    raw: RegisterRequest
    ){
    const rawPassword = raw.password
    const req = await this.preCreate(raw)

    let transaction
    let data
    try {
      transaction = await this.db.transaction({
        isolationLevel: Transaction.ISOLATION_LEVELS.READ_UNCOMMITTED,
      })      
            
      data = await User.create(req, {transaction})

      raw.password = rawPassword
      raw.store.userId = data.id                  

      await transaction.commit()
    } catch(error){
      await transaction.rollback()      
      throw new Error()
    }    

    await this.postCreate(raw)

    return data
  }

  async postCreate(
    req:RegisterRequest,    
  ){    
    const loginCred = await this.login({
      email:req.email,
      password:req.password
    })
    
    await this.requestHelper.makeRequest(
      loginCred.token,
      METHOD_POST,
      STORE_SERVICE,
      ACTION_STORE,
      null,   
      null,   
      req.store
    )    
  }

  async update(id:number, raw:UpdateRequest){
    let user = await this.findOne(id, false, true)
    user = await this.preUpdate(raw, user)    
    let transaction
    let data

    try {
      transaction = await this.db.transaction({
        isolationLevel: Transaction.ISOLATION_LEVELS.READ_UNCOMMITTED,
      })            
      
      data = await user.update({
        phone: raw.phone,   
        imageUrl: raw.imageUrl,
        userName: raw.userName,  
      }, {transaction})  

      await transaction.commit()
    } catch(error){
      await transaction.rollback()
      throw new Error()
    }

    return data    
  }

  async login(req: LoginRequest){    
    const user = await this.findOneByEmail(req.email, false, false)
    if(!user){
      throw new UnprocessableEntityException('user dengan credential berikut tidak ditemukan')
    }
    const returnPassword = await this.passwordHasher.compare(req.password, user.password)
    if(!returnPassword){
      throw new UnprocessableEntityException('user dengan credential berikut tidak ditemukan')
    }

    const payload = {
      id: user.id,
      companyId:null,
      userName: user.userName,
      phone: user.phone,
      email: user.email,
      imageUrl: user.imageUrl,   
      token:null   
    }
    const accessToken = await this.authService.createToken(payload)
    payload.token = accessToken.token
    return payload
  }
}