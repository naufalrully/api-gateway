import { Model, Table, Column, DataType, CreatedAt, UpdatedAt, ForeignKey, BelongsTo, HasOne, HasMany } from 'sequelize-typescript';
import { Product } from './Product';
import { User } from './User';
// import { Product } from './Product';
// import { User } from './User';

@Table({
  tableName: 'store'
})

export class Store extends Model {  
  @Column({
    type: DataType.BIGINT,
    primaryKey: true,
    field: 'id',
    autoIncrement: true,
  })  
  public id: number

  @Column({
    field: 'name',
  })
  public name: string

  @Column({
    field: 'address',
  })
  public address: string

  @Column({
    field: 'phone',
  })
  public phone: string

  @Column({
    field: 'email',
  })
  public email: string

  @Column({
    field: 'image_url',
  })
  public imageUrl: string

  @ForeignKey(() => User)
  @Column({
    field: 'user_id',
  })
  public userId: number
 
  @Column({
    type: DataType.DATE,
    field: 'created_at',
  })
  @CreatedAt
  public createdAt: Date

  @Column({
    type: DataType.DATE,
    field: 'updated_at',
  })
  @UpdatedAt
  public updatedAt: Date

  @Column({
    type: DataType.BOOLEAN,
    field: 'is_deleted',
  })
  public isDeleted: boolean 

  @HasMany(() => Product)
  public product: Product

  @BelongsTo(() => User)
  public user: User

}
