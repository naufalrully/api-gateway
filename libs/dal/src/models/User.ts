import { Model, Table, Column, DataType, CreatedAt, UpdatedAt, HasMany, ForeignKey } from 'sequelize-typescript';
import { Product } from './Product';
import { Store } from './Store';
// import { Product } from './Product';
// import { Store } from './Store';

@Table({
  tableName: 'user',
})
export class User extends Model{

  // @ForeignKey(() => Product)
  // @ForeignKey(() => Store)
  @Column({
    type: DataType.BIGINT,
    primaryKey: true,
    field: 'id',
    autoIncrement: true,
  })  
  public id: number

  @Column({
    field: 'user_name',
  })
  public userName: string

  @Column({
    field: 'password',
  })
  public password: string

  @Column({
    field: 'phone',
  })
  public phone: string

  @Column({
    field: 'email',
  })
  public email: string

  @Column({
    field: 'image_url',
  })
  public imageUrl: string
 
  @Column({
    type: DataType.DATE,
    field: 'created_at',
  })
  @CreatedAt
  public createdAt: Date

  @Column({
    type: DataType.DATE,
    field: 'updated_at',
  })
  @UpdatedAt
  public updatedAt: Date

  @Column({
    type: DataType.BOOLEAN,
    field: 'is_deleted',
  })
  public isDeleted: boolean 

  @HasMany(() => Store)
  public store: Store

  @HasMany(() => Product)
  public product: Product

}
