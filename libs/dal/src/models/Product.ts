import { Model, Table, Column, DataType, CreatedAt, UpdatedAt, ForeignKey, BelongsTo, HasOne } from 'sequelize-typescript';
import { Store } from './Store';
import { User } from './User';
// import { Store } from './Store';
// import { User } from './User';

@Table({
  tableName: 'product',
})
export class Product extends Model{
  @Column({
    type: DataType.BIGINT,
    primaryKey: true,
    field: 'id',
    autoIncrement: true,
  })  
  public id: number

  @Column({
    field: 'name',
  })
  public name: string

  @Column({
    field: 'description',
  })
  public description: string

  @Column({
    field: 'image_url',
  })
  public imageUrl: string

  @ForeignKey(() => User)
  @Column({
    field: 'user_id',
  })
  public userId: number

  @ForeignKey(() => Store)
  @Column({
    field: 'store_id',
  })
  public storeId: number
 
  @Column({
    type: DataType.DATE,
    field: 'created_at',
  })
  @CreatedAt
  public createdAt: Date

  @Column({
    type: DataType.DATE,
    field: 'updated_at',
  })
  @UpdatedAt
  public updatedAt: Date

  @Column({
    type: DataType.BOOLEAN,
    field: 'is_deleted',
  })
  public isDeleted: boolean 

  @BelongsTo(() => Store)
  public store: Store

  @BelongsTo(() => User)
  public user: User

}
