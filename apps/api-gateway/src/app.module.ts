import { CommonModule } from 'app/common';
import { Module } from '@nestjs/common';
import { ProductController } from './controllers/product.controller';
import { StoreController } from './controllers/store.controller';
import { UserController } from './controllers/user.controller';
import { BllModule } from 'libs/bll/src';
import { ConfigService } from 'app/common/configs/config.service';
import { JwtModule } from '@nestjs/jwt';
import { JwtStrategy } from 'app/common/strategies/jwt.strategy';
import * as fs from 'fs';
import { PassportModule } from '@nestjs/passport';

@Module({
  imports: [
    BllModule,
    CommonModule,       
    ConfigService,  
    PassportModule.register({ defaultStrategy: 'jwt' }),
    JwtModule.registerAsync({
      imports: [
        ConfigService,
        CommonModule,
      ],
      useFactory: async (configService: ConfigService) => ({                
        privateKey: fs.readFileSync(configService.get('JWT_KEY_FOLDER') + configService.get('JWT_SECRET_KEY')),
        publicKey: fs.readFileSync(configService.get('JWT_KEY_FOLDER') + configService.get('JWT_PUBLIC_KEY')),
        verifyOptions: { 
          algorithms: [configService.get('JWT_ALGORITHM')] 
        },
        signOptions: { 
          expiresIn: configService.get('JWT_DEFAULT_EXPIRE_TIME'), 
          algorithm:configService.get('JWT_ALGORITHM') 
        },
      }),
      inject: [ConfigService],      
    }),      
  ],
  controllers: [    
    ProductController,
    StoreController,    
    UserController,
  ],
  providers: [
    JwtStrategy,
  ],
})
export class AppModule {}
