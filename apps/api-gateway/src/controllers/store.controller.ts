import { StoreRequest } from 'app/common/requests/store.request';
import { Body, Controller, Delete, Get, Param, ParseIntPipe, Post, Put, Query, UseGuards, Headers, Request } from '@nestjs/common';
import { RequestHelper } from 'app/common/helpers/request.helper';
import { ACTION_STORE, METHOD_DELETE, METHOD_GET, METHOD_POST, METHOD_PUT, STORE_SERVICE } from 'app/common/helpers/constant';
import { circularToJSON } from 'app/common/helpers/helper';
import { JwtAuthGuard } from 'app/common/guards/jwt-auth.guard';

@Controller('store')
export class StoreController {
  constructor(
    private readonly requestHelper: RequestHelper
  ) {}

  @Get()
  async listStore(@Query() query){
    return await this.requestHelper.makeRequest(
      null,
      METHOD_GET,
      STORE_SERVICE,
      ACTION_STORE,
      null,
      query      
    )    
  }

  @UseGuards(JwtAuthGuard)
  @Post()
  async createProduct(
    @Request() req,
    @Body() body: StoreRequest,
    @Headers('authorization') token,
  ){    
    return await this.requestHelper.makeRequest(
      token,
      METHOD_POST,
      STORE_SERVICE,
      ACTION_STORE,
      null,   
      null,   
      body
    )    
  }

  @Get(':id')
  async getStore(
    @Param('id', ParseIntPipe) id:number,
    @Query() query,
  ){    
    return await this.requestHelper.makeRequest(
      null,
      METHOD_GET,
      STORE_SERVICE,
      ACTION_STORE,
      id,   
      query,   
    )    
  }

  @UseGuards(JwtAuthGuard)
  @Put(':id')
  async putStore(
    @Param('id', ParseIntPipe) id:number,    
    @Body() body: StoreRequest,
    @Headers('authorization') token,
  ){    
    return await this.requestHelper.makeRequest(
      token,
      METHOD_PUT,
      STORE_SERVICE,
      ACTION_STORE,
      id,   
      null,   
      body
    )    
  }

  @UseGuards(JwtAuthGuard)
  @Delete(':id')
  async deleteStore(
    @Param('id', ParseIntPipe) id:number,        
    @Headers('authorization') token,
  ){    
    return await this.requestHelper.makeRequest(
      token,
      METHOD_DELETE,
      STORE_SERVICE,
      ACTION_STORE,
      id,   
      null,   
      null
    )    
  }
}
