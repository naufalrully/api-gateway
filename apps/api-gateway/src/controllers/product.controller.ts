import { Body, Controller, Delete, Get, Param, ParseIntPipe, Post, Put, Query, UseGuards, Request, Headers } from '@nestjs/common';
import { JwtAuthGuard } from 'app/common/guards/jwt-auth.guard';
import { ACTION_PRODUCT, METHOD_DELETE, METHOD_GET, METHOD_POST, METHOD_PUT, PRODUCT_SERVICE } from 'app/common/helpers/constant';
import { circularToJSON } from 'app/common/helpers/helper';
import { RequestHelper } from 'app/common/helpers/request.helper';
import { ProductRequest } from 'app/common/requests/product.request';
import { query } from 'express';

@Controller('product')
export class ProductController {
  constructor(    
    private readonly requestHelper: RequestHelper
  ) {}

  @Get()
  async listProduct(@Query() query){
    return await this.requestHelper.makeRequest(
      null,
      METHOD_GET,
      PRODUCT_SERVICE,
      ACTION_PRODUCT,
      null,
      query      
    )    
  }

  @UseGuards(JwtAuthGuard)
  @Post()
  async createProduct(
    @Request() req,
    @Body() body: ProductRequest,
    @Headers('authorization') token,
  ){    
    return await this.requestHelper.makeRequest(
      token,
      METHOD_POST,
      PRODUCT_SERVICE,
      ACTION_PRODUCT,
      null,   
      query,   
      body
    )    
  }

  @Get(':id')
  async getProduct(
    @Param('id', ParseIntPipe) id:number,
    @Query() query,
  ){    
    return await this.requestHelper.makeRequest(
      null,
      METHOD_GET,
      PRODUCT_SERVICE,
      ACTION_PRODUCT,
      id,   
      query,   
    )    
  }

  @UseGuards(JwtAuthGuard)
  @Put(':id')
  async putProduct(
    @Param('id', ParseIntPipe) id:number,    
    @Body() body: ProductRequest,
    @Headers('authorization') token,
  ){    
    return await this.requestHelper.makeRequest(
      token,
      METHOD_PUT,
      PRODUCT_SERVICE,
      ACTION_PRODUCT,
      id,   
      null,   
      body
    )    
  }

  @UseGuards(JwtAuthGuard)
  @Delete(':id')
  async deleteProduct(
    @Param('id', ParseIntPipe) id:number,        
    @Headers('authorization') token,
  ){    
    return await this.requestHelper.makeRequest(
      token,
      METHOD_DELETE,
      PRODUCT_SERVICE,
      ACTION_PRODUCT,
      id,   
      null,   
      null
    )    
  }
}
