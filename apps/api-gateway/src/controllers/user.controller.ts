// import { RegisterRequest, LoginRequest } from 'app/common/requests/user.request';
import { Body, Controller, Delete, Get, Param, ParseIntPipe, Post, Put, Query, UseGuards, Headers, Request } from '@nestjs/common';
import { JwtAuthGuard } from 'app/common/guards/jwt-auth.guard';
import { ACTION_USER, ACTION_USER_LOGIN, METHOD_GET, METHOD_POST, METHOD_PUT, USER_SERVICE } from 'app/common/helpers/constant';
import { RequestHelper } from 'app/common/helpers/request.helper';
import { LoginRequest, RegisterRequest, UpdateRequest } from 'app/common/requests/user.request';


@Controller('user')
export class UserController {
  constructor(
    private readonly requestHelper: RequestHelper
  ) {}

  @UseGuards(JwtAuthGuard)
  @Get()
  async validate(    
    @Query() query,
    @Headers('authorization') token,
  ){        
    return await this.requestHelper.makeRequest(
      token,
      METHOD_GET,
      USER_SERVICE,
      ACTION_USER,
      null,   
      query,   
    )    
  }

  @UseGuards(JwtAuthGuard)
  @Get(':id')
  async getUser(
    @Param('id', ParseIntPipe) id:number,
    @Query() query,
    @Headers('authorization') token,
  ){        
    return await this.requestHelper.makeRequest(
      token,
      METHOD_GET,
      USER_SERVICE,
      ACTION_USER,
      id,   
      query,   
    )    
  }

  @UseGuards(JwtAuthGuard)
  @Put('')
  async putUser(    
    @Body() body: UpdateRequest,
    @Headers('authorization') token,    
  ){    
    return await this.requestHelper.makeRequest(
      token,
      METHOD_PUT,
      USER_SERVICE,
      ACTION_USER,
      null,   
      null,   
      body
    )    
  }

  @Post('login')
  async login(
    @Body() body: LoginRequest
  ){
    return await this.requestHelper.makeRequest(
      null,
      METHOD_POST,
      USER_SERVICE,
      ACTION_USER_LOGIN,
      null,
      null,
      body                     
    ) 
  }

  @Post()
  async register(
    @Body() body: RegisterRequest
  ){
    return await this.requestHelper.makeRequest(
      null,
      METHOD_POST,
      USER_SERVICE,
      ACTION_USER,
      null,
      null,
      body                     
    ) 
  } 
}
