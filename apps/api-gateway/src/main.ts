

import { NestFactory } from '@nestjs/core';
import { ConfigService } from 'app/common/configs/config.service';
import { AllExceptionsFilter } from 'app/common/filters/all-exception.filter';
import { ValidationPipe } from 'app/common/pipes/validation.pipe';
import { AppModule } from './app.module';
import { install } from 'source-map-support';
import { dbPath } from 'libs/dal/src/dal.module';

async function bootstrap() {
  install();
  const config = new ConfigService()
  config.dbConnection().addModels([dbPath])
  const app = await NestFactory.create(AppModule);  
  app.useGlobalFilters(new AllExceptionsFilter)
  app.useGlobalPipes(new ValidationPipe())
  app.enableCors({
    origin: '*',
    methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
    allowedHeaders: '*',
    credentials: false,
  })

  await app.listen(config.get('API_GATEWAY_PORT'));
}
bootstrap();
