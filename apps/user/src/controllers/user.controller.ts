import { RegisterRequest, LoginRequest, UserViewModel, UpdateRequest } from 'app/common/requests/user.request';
import { Body, Controller, Delete, Get, Param, ParseIntPipe, Patch, Post, Put, Query, Request, UseGuards } from '@nestjs/common';
import { UserService } from 'libs/bll/src/services/user.service';
import { BaseResource } from 'app/common/helpers/base.resource';
import { circularToJSON } from 'app/common/helpers/helper';
import { JwtAuthGuard } from 'app/common/guards/jwt-auth.guard';

@Controller('user')
export class UserController {
  constructor(
    private service: UserService
  ) {}

  @UseGuards(JwtAuthGuard)
  @Get()
  async listUser(@Request() req){    
    return new BaseResource(
      circularToJSON(
        new UserViewModel(
          await this.service.findOne(req.user.id, false, true)
        )
      ),
      'user'
    )    
  }

  @Post('login')
  async login(@Body() body: LoginRequest){
    return new BaseResource(
      circularToJSON(
        await this.service.login(body)
      ),
      'user'
    )    
  }

  @Post()
  async register(@Body() body: RegisterRequest){
    return new BaseResource(
      circularToJSON(
        new UserViewModel(
          await this.service.create(body)
        ) 
      ),
      'user'
    )    
  }  

  @UseGuards(JwtAuthGuard)
  @Get(':id')
  async getUser(@Param('id', ParseIntPipe) id:number){
    return new BaseResource(
      circularToJSON(
        new UserViewModel(
          await this.service.findOne(id, false, true)
        )
      ), 
      'user'
    )    
  }

  @UseGuards(JwtAuthGuard)
  @Put()
  async putUser(@Request() req, @Body() body: UpdateRequest){    
    return new BaseResource(
      circularToJSON(
        new UserViewModel(
          await this.service.update(Number(req.user.id), body)
        )
      ), 
      'user'
    )    
  }
  
}
