import { Body, Controller, Delete, Get, Param, ParseIntPipe, Patch, Post, Put, Query, Request, UseGuards } from '@nestjs/common';
import { BaseResource } from 'app/common/helpers/base.resource';
import { circularToJSON } from 'app/common/helpers/helper';
import { JwtAuthGuard } from 'app/common/guards/jwt-auth.guard';
import { StoreService } from 'libs/bll/src/services/store.service';
import { StoreRequest } from 'app/common/requests/store.request';
import { request } from 'express';
import { identity } from 'rxjs';

@Controller('store')
export class StoreController {
  constructor(
    private service: StoreService
  ) {}

  // @UseGuards(JwtAuthGuard)
  @Get()
  async listStore(@Query() query){        
    return new BaseResource(
      circularToJSON(
        await this.service.findAll(query, false)
      ),
      'store'
    )    
  }

  @Get(':id')
  async getStore(
    @Param('id', ParseIntPipe) id:number,
    @Query() query
    ){
    return new BaseResource(
      circularToJSON(        
        await this.service.findOne(id, query, false, true)        
      ), 
      'store'
    )    
  }

  @UseGuards(JwtAuthGuard)
  @Post()
  async createStore(
    @Request() req,
    @Body() body: StoreRequest
  ){    
    return new BaseResource(
      circularToJSON(
        await this.service.create(req.user, body)        
      ),
      'store'
    )    
  }

  @UseGuards(JwtAuthGuard)
  @Put(':id')
  async updateStore(
    @Request() req,
    @Param('id', ParseIntPipe) id,
    @Body() body: StoreRequest,
  ){    
    return new BaseResource(
      circularToJSON(
        await this.service.update(req.user, id, body)        
      ),
      'store'
    )    
  }

  @UseGuards(JwtAuthGuard)
  @Delete(':id')
  async deleteStore(
    @Request() req,
    @Param('id', ParseIntPipe) id,
  ){    
    return new BaseResource(
      circularToJSON(
        await this.service.delete(req.user, id)        
      ),
      'store'
    )    
  }
  
  
}
