import { NestFactory } from '@nestjs/core';
import { ConfigService } from 'app/common/configs/config.service';
import { AllExceptionsFilter } from 'app/common/filters/all-exception.filter';
import { ValidationPipe } from 'app/common/pipes/validation.pipe';
import { dbPath } from 'libs/dal/src';
import { ProductModule } from './product.module';

async function bootstrap() {
  const config = new ConfigService()
  config.dbConnection().addModels([dbPath])
  
  const app = await NestFactory.create(ProductModule);
  app.useGlobalFilters(new AllExceptionsFilter)
  app.useGlobalPipes(new ValidationPipe())
  app.enableCors({
    origin: '*',
    methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
    allowedHeaders: '*',
    credentials: false,
  })
  await app.listen(config.get('PRODUCT_APP_PORT'));
}
bootstrap();
