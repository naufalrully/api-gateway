import { Body, Controller, Delete, Get, Param, ParseIntPipe, Patch, Post, Put, Query, Request, UseGuards } from '@nestjs/common';
import { BaseResource } from 'app/common/helpers/base.resource';
import { circularToJSON } from 'app/common/helpers/helper';
import { JwtAuthGuard } from 'app/common/guards/jwt-auth.guard';
import { ProductRequest } from 'app/common/requests/product.request';
import { request } from 'express';
import { identity } from 'rxjs';
import { ProductService } from 'libs/bll/src/services/product.service';

@Controller('product')
export class ProductController {
  constructor(
    private service: ProductService,
  ) {}

  // @UseGuards(JwtAuthGuard)
  @Get()
  async listProduct(@Query() query){        
    return new BaseResource(
      circularToJSON(
        await this.service.findAll(query, false)
      ),
      'product'
    )    
  }

  @Get(':id')
  async getProduct(
    @Param('id', ParseIntPipe) id:number,
    @Query() query
    ){
    return new BaseResource(
      circularToJSON(        
        await this.service.findOne(id, query, false, true)        
      ), 
      'product'
    )    
  }

  @UseGuards(JwtAuthGuard)
  @Post()
  async createProduct(
    @Request() req,
    @Body() body: ProductRequest
  ){    
    return new BaseResource(
      circularToJSON(
        await this.service.create(req.user, body)        
      ),
      'product'
    )    
  }

  @UseGuards(JwtAuthGuard)
  @Put(':id')
  async updateProduct(
    @Request() req,
    @Param('id', ParseIntPipe) id,
    @Body() body: ProductRequest,
  ){    
    return new BaseResource(
      circularToJSON(
        await this.service.update(req.user, id, body)        
      ),
      'product'
    )    
  }

  @UseGuards(JwtAuthGuard)
  @Delete(':id')
  async deleteProduct(
    @Request() req,
    @Param('id', ParseIntPipe) id,
  ){    
    return new BaseResource(
      circularToJSON(
        await this.service.delete(req.user, id)        
      ),
      'product'
    )    
  }
  
  
}
